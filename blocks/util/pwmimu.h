#ifndef PWMIMU_H
#define PWMIMU_H

#include <stdint.h>

/**
 * Converts a pwm reading to roll or pitch assuming A2 gimbal is set up to
 * output from [-14000,14000], centered at -560, and with gains set to 20%
 */
inline double a2_pwm_to_rp(int16_t pwm);

/**
 * Converts a roll or pitch to a pwm reading assuming A2 gimbal is set up to
 * output from [-14000,14000], centered at -560, and with gains set to 20%
 */
inline int16_t a2_rp_to_pwm(double rp);

#endif /* end of include guard: PWMIMU_H */
