#include <math.h>
#include <float.h>

#include "pwmimu.h"

#ifndef M_PI
#define M_PI 3.14159265359
#endif

#define M_RPDEG2PWM 14.6
static const double      m_rp2pwm = (180.0/M_PI) * M_RPDEG2PWM;
static const double      m_pwm2rp = (M_PI/180.0) / M_RPDEG2PWM;
static const double      rp0      = 0.0;
static const int16_t     pwm0     = 1500;

inline double a2_pwm_to_rp(int16_t pwm)
{
    if(pwm < 500 || pwm > 2500) return DBL_MAX;
    return m_pwm2rp * (pwm - pwm0) + rp0;
}

inline int16_t a2_rp_to_pwm(double rp)
{
    if(rp < -2*M_PI || rp > 2*M_PI) return INT16_MAX;
    return m_rp2pwm * (rp - rp0) + pwm0;
}
